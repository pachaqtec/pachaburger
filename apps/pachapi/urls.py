from django.urls import include, path
from rest_framework import routers
from .views import SupplyList, SupplyDetail, \
    OrderCocina, OrderList, \
    CustomUserCreate

#router = routers.DefaultRouter()
#router.register(r'supply', views.SupplyViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('supply/',SupplyList.as_view(), name='supply_list'),
    path('supply/<pk>',SupplyDetail.as_view(), name='supply_detalle'),

    path('order/', OrderList.as_view(), name='order_list'),
    path('orderCocina/', OrderCocina.as_view(), name='order_cocina'),


    path('user/', CustomUserCreate.as_view(), name='user_create'),
    #path('', include(router.urls)),
    #path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]

