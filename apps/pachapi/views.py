from django.shortcuts import render

from rest_framework.views import APIView
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from rest_framework import generics

from .models import Supply, Order
from .serializers import SupplySerializer, OrderSerializer, \
        CustomUserSerializer


class SupplyList(generics.ListCreateAPIView):
    queryset = Supply.objects.all()
    serializer_class = SupplySerializer

class SupplyDetail(generics.RetrieveDestroyAPIView):
    queryset = Supply.objects.all()
    serializer_class = SupplySerializer

class OrderSave(generics.CreateAPIView):
    serializer_class = OrderSerializer

class OrderList(generics.ListCreateAPIView):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer

class OrderDetail(generics.RetrieveDestroyAPIView):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer

class OrderCocina(generics.ListCreateAPIView):
    queryset = Order.objects.filter(status="REGISTRADO")
    serializer_class = OrderSerializer

#class OrderCocinaView(APIView):
#    def get(self, request):
#        orders = Order.objects.all()
#        return Response({"orders": orders})

class CustomUserCreate(generics.CreateAPIView):
    serializer_class = CustomUserSerializer


#class SupplyList(APIView):
#    def get(self, request):
#        supp = Supply.objects.all()
#        data = SupplySerializer(supp,many=True).data
#        return Response(data)

#class SupplyDetail(APIView):
#    def get(self, request, id):
#        supp = get_object_or_404(Supply,id=id)
#        data = SupplySerializer(supp).data
#        return Response(data)
