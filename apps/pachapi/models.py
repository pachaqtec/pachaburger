from django.db import models
from django.contrib.auth.models import AbstractUser

class Role(models.Model):
    name = models.CharField(max_length=50, unique=True, blank=False, null=False)
    status = models.BooleanField(default=True)
    
    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Roles"
        verbose_name = "Role"


class CustomUser(AbstractUser):
    age = models.PositiveIntegerField(null=True, blank=True)
    role = models.ForeignKey(Role, null=True, blank=True, on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = "CustomUsers"
        verbose_name = "CustomUser"


class Supply(models.Model):
    name = models.CharField(max_length=50, unique=True, blank=False, null=False)
    price = models.FloatField(default=0)
    quantity = models.PositiveIntegerField()
    image = models.ImageField(upload_to='supplies/%Y/%m/%d')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "Supplies"
        verbose_name = "Supply"


class Order(models.Model):
    REGISTRADO = 'REGISTRADO'
    EN_PROCESO = 'EN PROCESO'
    PREPARADO = 'PREPARADO'
    ENTREGADO = 'ENTREGADO'
    STATUS_CHOICES = [
        (REGISTRADO,'registrado'),
        (EN_PROCESO,'proceso'),
        (PREPARADO,'preparado'),
        (ENTREGADO,'entregado'),
    ]

    cod_order = models.CharField(max_length=20)
    cliente = models.CharField(max_length=100, default='')
    fecha = models.DateTimeField(auto_now_add=True)
    price = models.FloatField(default=0)
    status = models.CharField(max_length=10,choices=STATUS_CHOICES, default=REGISTRADO)

    class Meta:
        verbose_name_plural = "Encabezado Ordenes"
        verbose_name = "Encabezado Orden"

    def __str__(self):
        return 'Order {}'.format(self.id)

    def set_total_order(self):
        self.price = sum(detail.get_cost() for detail in self.orderDetails.all())


class OrderDetail(models.Model):
    order = models.ForeignKey(Order, related_name='orderDetails', on_delete=models.CASCADE)
    supply = models.ForeignKey(Supply, related_name='order_items', on_delete=models.CASCADE)
    quantity = models.BigIntegerField(default=1)

    class Meta:
        unique_together = ['order','supply']
        verbose_name_plural = "Detalle Ordenes"
        verbose_name = "Detalle Orden"

    def __str__(self):
        return '{}'.format(self.supply)

    def get_cost(self):
        return self.quantity * self.supply.price

