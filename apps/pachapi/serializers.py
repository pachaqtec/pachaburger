from rest_framework import serializers
from .models import Supply, Order, OrderDetail, CustomUser

class SupplySerializer(serializers.ModelSerializer):

    class Meta:
        model = Supply
        fields = ('id', 'name', 'price', 'quantity', 'image')

class OrderDetailSerializer(serializers.ModelSerializer):
    supply = SupplySerializer(read_only=True)
    class Meta:
        model = OrderDetail
        fields = '__all__'


class OrderSerializer(serializers.ModelSerializer):
    orderDetails = OrderDetailSerializer(many=True)

    class Meta:
        model = Order
        fields = ('id', 'cod_order', 'cliente', 'fecha', 'price', 'status', 'orderDetails')

    def create(self, validated_data):
        orderDetails_data = validated_data.pop('orderDetails')
        order = Order.objects.create(**validated_data)
        for orderDetail_data in orderDetails_data:
            OrderDetail.objects.create(order=order, **orderDetail_data)
        return order

class OrderCocinaSerializer(serializers.Serializer):
    pass


class CustomUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = ('username', 'email', 'password')
        extra_kwargs = {'password':{'write_only':True}}

        def create(self, validated_data):
            customUser = CustomUser(
                    email = validated_data['email'],
                    username = validated_data['username']
                    )
            customUser.is_staff = True      
            customUser.set_password(validated_data['password'])
            customUser.save()
            return customUser
