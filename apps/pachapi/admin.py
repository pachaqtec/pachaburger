from django.contrib import admin
from .models import Supply
from .models import CustomUser, Role
from .models import Order, OrderDetail

admin.site.register(Role)
admin.site.register(CustomUser)
admin.site.register(Supply)
admin.site.register(OrderDetail)

class OrderDetailInline(admin.TabularInline):
    model = OrderDetail
    raw_id_fields = ['supply']

@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ['id','cod_order','cliente',
                    'fecha','price','status']
    list_filter = ['status']
    inline = [OrderDetailInline]